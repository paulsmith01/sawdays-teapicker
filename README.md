
# Sawdays - Tea Round Picker

This project uses a MEAN stack, with a remote MongoDB database (hosted by https://mlab.com/), a simple CRUD API built with Express, and a web front-end build in Angular 6. This is loosely based on a tutorial I have been working from, but modified by me to fit the brief.

Just in case you're not able to get this installed, I have deployed it to Heroku at: https://sawdays-tea.herokuapp.com/ It may take a short while for the dyno to initialise and the tool to start up, as the app needs to be built when it's not been used for a while as it's running on the free 'Hobby' tier. Try reloading after a minute or so if nothing seems to be happening.

## Process

I decided I wanted to try and build this tool using some technology I was less familiar with in order to give myself a challenge, and demonstrate that I could learn and produce something relatively quickly.

I wanted to build a minimum-viable product that fitted the brief, so decided that it should support these features:

 - Add / edit / delete people for the tea round, storing people in a database to they don't all have to be added each time
 - Store their name, preferred beverage, and team
 - Randomly pick and highlight a team member when the tool is loaded

## Requirements for installation

The following tools, frameworks, and modules are required for this tool:

- Node.js
- Angular CLI
- Angular 6
- Express.js
- Mongoose.js

(MongoDB is set up on a remote host so no need to install this)

## Build and run

To download the source from this repository, run `git clone https://paulsmith01@bitbucket.org/paulsmith01/sawdays-teapicker.git`

Run `npm install` to install package dependencies

Run `npm start` to build the project. Server should start on `http://localhost:3000`

## How it works

When the tool is initialised, a request is made via the API to load all the 'people' records stored in the MongoDB database. It counts the number of people in the returned object, and using this value it selects one at random using a Math.random function scoped to the number of people, and converts this to an integer with Math.floor. This value is then used to reference the ID of one of the people in the object returned from the database, and the row with this ID is then highlighted in the table. Reloading the page will refresh the list.

More people can be added using the [+] button. Clicking on an individual in the table allows their entry to be edited and saved back to the database, or alternatively you can delete this person. 

The tool uses Angular Material https://material.angular.io/ for the UI.

## If more time...

I would have utilised the fact that I am storing the person's team to have an initial view to allow the selection of the team for which a particular round is being decided. The team name addition would have been done via a select list to prevent unexpected variations (e.g. Web TEAM vs. Web team, etc).

The current selection process is completely random, so it is possible that the same person can be chosen in consecutive runs. I would add a further field of 'date last picked' and include this in the randomiser function to make sure that the last person to make tea is not included in the algorithm to select the next person, or alternatively that everyone makes tea before a turn comes round again.


## Final note

I have hardcoded the database username/password in to the code. This is just for convenience for the demo - I would not normally store these in the repository, and would use config files to handle this for the various environments being deployed to.