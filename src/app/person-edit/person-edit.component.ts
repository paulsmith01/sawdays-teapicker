import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-person-edit',
  templateUrl: './person-edit.component.html',
  styleUrls: ['./person-edit.component.css']
})
export class PersonEditComponent implements OnInit {

  personForm: FormGroup;
  id:string = '';
  name:string = '';
  beverage:string = '';
  team:string = '';

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getPerson(this.route.snapshot.params['id']);
    this.personForm = this.formBuilder.group({
      'name' : [null, Validators.required],
      'beverage' : [null, Validators.required],
      'team' : [null, Validators.required],
    });
  }

  getPerson(id) {
    this.api.getPerson(id).subscribe(data => {
      this.id = data._id;
      this.personForm.setValue({
        name: data.name,
        beverage: data.beverage,
        team: data.team
      });
    });
  }

  onFormSubmit(form:NgForm) {
    this.api.updatePerson(this.id, form)
      .subscribe(res => {
          let id = res['_id'];
          this.router.navigate(['/person-details', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }

  personDetails() {
    this.router.navigate(['/person-details', this.id]);
  }
}
