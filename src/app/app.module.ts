import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { PersonComponent } from './person/person.component';
import { PersonCreateComponent } from './person-create/person-create.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule } from "@angular/material";
import { PersonEditComponent } from './person-edit/person-edit.component';
import { PersonDetailsComponent } from './person-details/person-details.component';

const appRoutes: Routes = [
  {
    path: '',
    component: PersonComponent,
    data: { title: 'Person List' }
  },
  {
    path: 'person-details/:id',
    component: PersonDetailsComponent,
    data: { title: 'Person Details' }
  },
  {
    path: 'person-create',
    component: PersonCreateComponent,
    data: { title: 'Create Person' }
  },
  {
    path: 'person-edit/:id',
    component: PersonEditComponent,
    data: { title: 'Edit Person' }
  },
  // { path: '',
  //   redirectTo: '/persons',
  //   pathMatch: 'full'
  // }
];

@NgModule({
  declarations: [
    AppComponent,
    PersonComponent,
    PersonCreateComponent,
    PersonEditComponent,
    PersonDetailsComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }