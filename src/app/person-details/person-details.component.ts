import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.css']
})
export class PersonDetailsComponent implements OnInit {

  person = {};

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router) { }

  ngOnInit() {
    this.getPersonDetails(this.route.snapshot.params['id']);
  }

  getPersonDetails(id) {
    this.api.getPerson(id)
      .subscribe(data => {
        console.log(data);
        this.person = data;
      });
  }

  deletePerson(id) {
    this.api.deletePerson(id)
      .subscribe(res => {
          this.router.navigate(['/']);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
