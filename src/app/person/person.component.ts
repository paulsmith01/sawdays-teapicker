import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {


	displayedColumns = ['name', 'beverage'];
	dataSource = new PersonDataSource(this.api);
	num_people: number;
	highlight_id: number;
	peopleArray: any[];
	selected: number;
	random: number;

  	constructor(private api: ApiService) { }

	ngOnInit() {
	    this.api.getPeople()
	    .subscribe(res => {
	    	// Pick random number based on number of people, highlight this row in the table
	       	this.num_people = Object.keys(res).length;
	       	this.random = Math.floor(Math.random() * this.num_people);
	       	this.selected = res[this.random]._id;
	       	this.highlight(this.selected);
	    }, err => {
		   	console.log(err);
	    });
	}

	// Unset selected row
	selectedRowIndex: number = -1; 

	// Highlight selected row
	highlight(row){
	    this.selectedRowIndex = row;
	}

}

export class PersonDataSource extends DataSource<any> {
  constructor(private api: ApiService) {
    super()
  }

  connect() {
    return this.api.getPeople();
  }

  disconnect() {

  }
}

